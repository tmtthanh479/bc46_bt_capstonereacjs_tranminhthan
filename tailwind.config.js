/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.html', './src/**/*.jsx'],
  theme: {
    extend: {
      fontSize: {
        SupperMaxheader: "50px",
        MaxHeader: "30px",
        MinHeader: "22px",
        ContentMin: "15px",
      },
      width: {
        '100': '350px', 
        "99": "400px",
       
        
      
      
      },
      height: {
        '100': '550px',   
        "99": "400px"
        
      
      },
      colors: {
        red1: '#E25050',  
        bluebutton: "#1A9FFF",
        bluebuttonHover: "1A9FFF"
      },
      borderWidth: {
        "1" : "1px"
      },
      

    },
  },
  plugins: [],
}

