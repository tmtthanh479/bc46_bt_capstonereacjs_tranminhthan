import "./App.css";
import { Route, Routes } from "react-router-dom";
import { PATH } from "./config/path";
import MainLayout from "./components/layouts/MainLayout";
import Home from "./pages/Home";
import FoodAndDrink from "./pages/FoodAndDrink";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Theatre from "./pages/Theatre";
import Text from "./pages/Text";
import Myticket from "./pages/Myticket";
import MovieInfoDeital from "./pages/InfoAndSelectFilm/MovieInfoDeital";
import MovieInfoDeitalCenema from "./pages/InfoAndSelectFilm/MovieInfoDeitalCenema";
import AllMovie from "./pages/ShowingAndSoonMovie/AllMovie";
import BookTicket from "./pages/BookTicket/BookTicket";
import Result from "./pages/BookTicket/Result";
import Chair from "./pages/BookTicket/Chair";
import AllMovieSoon from "./pages/ShowingAndSoonMovie/AllMovieSoon";
import Cart from "./pages/Cart";
import DatVeSuccess from "./pages/DatVeSuccess";
import { NotFound } from "./pages/NotFound";
import ChairList from "./pages/BookTicket/ChairList";
import News from "./pages/News";
import Userinfomation from "./pages/Userinfomation";




function App() {
  return (
    <div>
      <Routes>
        <Route element={<MainLayout />}>
          <Route index  element={<Home />} />
          <Route path={PATH.theatre} element={<Theatre />} />
          <Route path={PATH.foodAndDrink} element={<FoodAndDrink />} />
          <Route path={PATH.login} element={<Login />} />
          <Route path={PATH.register} element={<Register />} />
          <Route path={PATH.text} element={<Text />} />
          <Route path={PATH.myticket} element={<Myticket />} />
          <Route path={PATH.MovieInfoDeital} element={<MovieInfoDeital />} />
          <Route path={PATH.MovieInfoDeitalCenema} element={<MovieInfoDeitalCenema />} />
          <Route path={PATH.AllMovie} element={<AllMovie />} />
          <Route path={PATH.AllMovieSoon} element={<AllMovieSoon />} />
          <Route path={PATH.BookTicket} element={<BookTicket />} />
          <Route path={PATH.ChairList} element={<ChairList />} />
          <Route path={PATH.Result} element={<Result />} />
          <Route path={PATH.Chair} element={<Chair />} />
          <Route path={PATH.Cart} element={<Cart />} />
          <Route path={PATH.DatVeSuccess} element={<DatVeSuccess />} />
          <Route path={PATH.News} element={<News />} />
          <Route path={PATH.Userinfomation} element={<Userinfomation />} />
      
        


        </Route>
        <Route path="*" element={<NotFound/>}>

        </Route>
      </Routes>
    </div>
  );
}

export default App;
