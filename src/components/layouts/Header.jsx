import React from "react";
import { Link, NavLink } from "react-router-dom";
import { PATH } from "../../config/path";
import "./style.scss";

const Sidebar = () => {
  return (
    <div className=" flex  max-w-[70%] mx-auto    ">
      <div className=" bg-gray-800 rounded-xl mt-5 w-[100%] flex p-5 justify-between   ">
        <Link to="/">MovieBooker</Link>
        <div className="space-x-28   ">
          <a href="">
            <NavLink to={PATH.AllMovie}>Movie </NavLink>
          </a>
          <a href="">
            <NavLink to={PATH.theatre}>Theatre </NavLink>
          </a>
          <a href="">
            <NavLink to={PATH.foodAndDrink}>Food & Drinks</NavLink>
          </a>
          <a href="">
            <NavLink to={PATH.AllMovieSoon}>Upcoming Movies</NavLink>
          </a>
        </div>
        <div className="flex space-x-16  ">
          <NavLink to={PATH.movie}>
            <img src={process.env.PUBLIC_URL + "/img/icon/search.png"} alt=".../" />
          </NavLink>
          <NavLink to={PATH.login}>
            <img src={process.env.PUBLIC_URL + "/img/icon/user.png"} alt=".../" />
          </NavLink>
          <NavLink to={PATH.myticket}>
            <img src={process.env.PUBLIC_URL + "/img/icon/ticketHeader.png"} alt=".../" />
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
