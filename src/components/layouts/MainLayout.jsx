// layout chính của web
import React from "react";
import Header from "../../components/layouts/Header";
import { Outlet } from "react-router-dom";
import Footer from "./Footer";


const MainLayout = () => {
  return (
    <div className="bg-[#0A0B0F] text-white " >
      {/* side bar */}
      <div   >
        <Header />
      </div>
      {/* content */}
      <div >
        <Outlet />
      </div>
      <div>
        <Footer/>
      </div>
    </div>
  );
};

export default MainLayout;
