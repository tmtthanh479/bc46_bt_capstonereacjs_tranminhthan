import React from "react";

const Theatre = () => {
  return (
    <div>
      {/* Main Banner */}
      <div>
        <img
          className="w-full object-cover h-[600px] flex mt-1  "
          src="./img/icon/MainIMG//wallpaperflare.com_wallpaper.jpg"
          alt=""
        />
      </div>
      <div>
        <div className="text-center mb-20  justify-between max-w-[70%] mx-auto mt-32  ">
          <h2 className="mb-20 text-MaxHeader font-bold">Specialty Theaters</h2>
          <div className="flex ">
            {/* 1 */}
            <div>
              <div class=" w-[650px]bg-[#0A0B0F]   ">
                <a href="#">
                  <img class="rounded" src="https://www.cgv.vn/media/imax/4DX_2.png" alt="" />
                </a>
                <div class="p-5 ">
                  <a href="#">
                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">4DX®,</h5>
                  </a>
                  <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-left">
                    A completely new cinematic format that awakens the senses of the audience, delivering extraordinary
                    movie experiences through state-of-the-art technology on a global scale.
                  </p>
                </div>
              </div>
            </div>
            {/* 2 */}
            <div>
              <div class="w-[650px] mt-16 ml-10 bg-[#0A0B0F]  ">
                <a href="#">
                  <img class="rounded" src="https://www.cgv.vn/media/imax/double-screen-imax.png" alt="" />
                </a>
                <div class="p-5">
                  <a href="#">
                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">IMAX</h5>
                  </a>
                  <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-left">
                    It's the most advanced film projection technology in the world today, from the incredibly immersive
                    theater design to the pinnacle of sound and visual effects.
                  </p>
                </div>
              </div>
            </div>
            {/* 3 */}
          </div>
          <div className="mr-auto justify-center flex mt-10">
            <img className="rounded" src="https://www.cgv.vn/media/imax/goldclass-1.png" alt="" />
            <p className="text-left ml-10 ">
              <span className="text-MaxHeader font-bold mr-2 ">Gold Class</span>{" "}
              <span className="mb-3 font-normal text-gray-700 dark:text-gray-400 ">
                Drawing inspiration from business class seats on airplanes, the GOLD CLASS cinema was born to provide a
                luxurious and upscale movie-watching environment for cinema enthusiasts. GOLD CLASS is also the perfect
                choice for special events, creating unforgettable memories in an exquisite setting
              </span>
            </p>
          </div>
          {/* 4 */}
          <div className="mr-auto justify-center flex mt-10">
            <p className="text-left mr-10">
              <span className="text-MaxHeader font-bold mr-2">L'amour</span>{" "}
              <span className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                With the aim of providing the sweetest moments and the utmost comfort while watching movies, CGV
                introduces the L'amour recliner theater, featuring the most luxurious interior design within the CGV
                cinema network, promising a fantastic experience for the audience
              </span>
            </p>
            <img className="rounded" src="https://www.cgv.vn/media/imax/lamour-2.png" alt="" />
          </div>
          <div className="flex  mt-10 ">
            {/* 5 */}
            <div>
              <div class=" w-[650px]bg-[#0A0B0F] mt-16   ">
                <a href="#">
                  <img class="rounded" src="https://www.cgv.vn/media/imax/starium-2.png" alt="" />
                </a>
                <div class="p-5">
                  <a href="#">
                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">STARIUM</h5>
                  </a>
                  <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-left">
                    STARIUM is a film projection technology created by CGV itself to push the audience's experiences
                    beyond the boundaries of reality.
                  </p>
                </div>
              </div>
            </div>
            {/* 6 */}
            <div>
              <div class="w-[650px]  ml-10 bg-[#0A0B0F]  ">
                <a href="#">
                  <img class="rounded" src="https://www.cgv.vn/media/theaters/special/screenx/screenx-3.jpg" alt="" />
                </a>
                <div class="p-5">
                  <a href="#">
                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">ScreenX</h5>
                  </a>
                  <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 text-left">
                    ScreenX transcends the confines of the main screen, expanding the visual space onto both side walls,
                    reaching all the way to the last row of seats.
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* 7 */}
          <div>
            <div className="mr-auto justify-center flex  ">
              <img className="rounded " src="https://www.cgv.vn/media/wysiwyg/2019/NOV19/livingroom2.jpg" alt="" />
            </div>
            <div className="">
              <h2 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white mt-10">Cine & Living</h2>
              <p className="mb-3 font-normal ">
                The Cine & Living cinema features a luxurious living room-style design, enhanced by the sharpness of the
                ONYX LED screen, delivering a vivid cinematic experience in every moment.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Theatre;
