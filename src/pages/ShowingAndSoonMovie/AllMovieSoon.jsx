import React, { useEffect, useState } from "react";
import { getMovies } from "../../services/QuanLyPhim";
import { Link, NavLink } from "react-router-dom";
import { PATH } from "../../config/path";

const AllMovieSoon = () => {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    getMovies()
      .then((response) => {
        setMovies(response.data.content);
      })
      .catch((error) => {
        console.error("Error fetching movie data:", error);
      });
  }, []);
  return (
    <>
      <div className="mr-auto my-20 max-w-[70%] mx-auto   rounded-xl ">
        <div className="text-MaxHeader text-red1 font-bold bg-[#14161d] rounded-xl p-5 ">Coming soon </div>
        <div className="bg-[#14161d] mt-10 rounded-xl">
          <div>
            <div className="grid grid-cols-4 gap-4 justify-between w-full">
              {/* car */}
              {movies
                .filter((e) => e.sapChieu)
                ?.map((e) => (
                  <div key={e.maPhim} className=" w-64 h-auto ml-8 m-5   ">
                    {e.hot ? <img className="h-12 absolute" src="./img/icon/MovieIcon/promotional.png" alt="" /> : null}
                    <img className="w-auto h-96 rounded shadow-2xl" src={e.hinhAnh} alt="" />
                    <p className="py-2 font-normal">{e.tenPhim}</p>
                    <div className="flex justify-between">
                      <Link to={`/MovieInfoDeital/${e.maPhim}`}>
                        <button
                        
                         className="mt-1 bg-red1  text-white px-3 py-2 rounded  hover:bg-red-800  duration-200">
                          Film Information
                        </button>
                      </Link>
                    </div>
                  </div>
                ))}

              {/*  */}
            </div>
            <div className=" text-right   ">
              <NavLink to={PATH.AllMovie}>
                <button className=" border-gray-500 border-2 mb-10 p-2 rounded mr-10  hover:scale-110 duration-200">
                  Now showing
                </button>
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AllMovieSoon;
