import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { movieActions } from "../../store/Movie/slice";
import { Link, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { getMovies } from "../../services/QuanLyPhim";

const Result = () => {
  const Booking = useSelector((state) => state.movie.Booking);
  const navigate = useNavigate();
  const dispath = useDispatch();
  const { id } = useParams();

  // Bắt user chọn 1 cái mới nhất rạp- location - showtime
  const latestInforFilmRap = useSelector((state) => state.movie.InforFilmRap[state.movie.InforFilmRap.length - 1]);
  const latestInforFilmTime = useSelector(
    (state) => state.movie.setBookinfoFilmTime[state.movie.setBookinfoFilmTime.length - 1]
  );
  const latestInforLocation = useSelector(
    (state) => state.movie.InforFilmLocation[state.movie.InforFilmLocation.length - 1]
  );

  // Tạo một mảng giả tạo nếu Booking là một mảng rỗng vẫn render
  const seatsToRender = Booking.length === 0 ? [{}] : Booking;
  // push seat uset chọn to Booking
  const [activeTab, setActiveTab] = useState(null);
  const handleTabClick = (tabIndex) => {
    setActiveTab(tabIndex);
  };

  //  toast
  const handleConfirmClick = () => {
    if (Booking.length > 0) {
      console.log("Confirmed");
    } else {
      toast.error("", {
        position: "top-right",
        autoClose: 5000,
        style: {
          background: "#14161d", // Đặt màu nền trực tiếp tại đây
          color: "white",
        },
      });
    }
  };

  // Điều hướng trở về trang trước
  const handleGoBack = () => {
    navigate(-1);
  };

  return (
    <>
      <div>
        {/* Ticket Information */}
        <section>
          <div className="p-8 w-[420px] rounded-2xl dark:bg-gray-800 dark:border-gray-700 ">
            <h2 className="text-MaxHeader  font-bold text-[#E25050]">Ticket Information</h2>

            {/* cinema */}
            <div className="flex mt-5 h-28 items-center  p-5 rounded-lg bg-[#2e3b4e] ">
              <img className="h-auto w-12 m-2  " src={process.env.PUBLIC_URL + "/img/icon/Cart/ticket.png"} alt="" />

              <h2 className=" ml-5">{latestInforLocation?.diaChi}</h2>
            </div>

            {/* Loai rạp */}
            <div className="flex mt-5  pl-5 p-1 rounded-lg bg-[#2e3b4e]  ">
              <img
                className=" h-auto w-12 m-2 "
                src={process.env.PUBLIC_URL + "/img/icon/BookTicketIcon/cinema.png"}
                alt=""
              />

              <h2 className=" m-3 ml-5 flex mr-auto items-center ">{latestInforFilmRap?.tenHeThongRap}</h2>
            </div>

            {/* suất chiếu  */}
            <div className="flex mt-5 pl-5 p-1 rounded-lg bg-[#2e3b4e] ">
              <img
                className=" h-auto w-12 m-2 "
                src={process.env.PUBLIC_URL + "/img/icon/BookTicketIcon/deadline.png"}
                alt=""
              />

              <h2 className="m-3 ml-5 flex mr-auto items-center">{latestInforFilmTime}</h2>
            </div>

            {/* Ghế*/}
            <div className="flex mt-5 pl-5 p-1 h-20 items-center rounded-lg bg-[#2e3b4e]">
              <img
                className="h-auto w-12 m-2"
                src={process.env.PUBLIC_URL + "/img/icon/BookTicketIcon/armchair.png"}
                alt=""
              />
              <div className="flex mr-auto items-center  overflow-x-auto ">
                {seatsToRender?.map((ghe) => (
                  <div
                    onClick={() => {
                      handleTabClick(0);
                    }}
                    key={ghe.soGhe}
                    className="rounded p-1 ml-5"
                  >
                    {ghe.soGhe}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </section>

        {/* Price */}
        <section>
          <div className=" mt-10  h-[434px] ">
            <div className="p-8 w-[420px]  rounded-2xl dark:bg-gray-800 dark:border-gray-700 ">
              <h2 className="text-MaxHeader  font-bold text-[#E25050]">Price</h2>

              <div className="flex mt-5 p-1 rounded-lg bg-[#2e3b4e] ">
                <p className="p-3 ">Tổng</p>

                <div>
                  <h2 className="p-3 ml-5 w-9">
                    {Booking.reduce((total, ghe) => (total += ghe.gia), 0).toLocaleString("vi-VN", {
                      style: "currency",
                      currency: "VND",
                    })}
                  </h2>
                </div>

                <button
                  onClick={(e) => {
                    dispath(movieActions.setDelBooking(e));
                  }}
                  className="ml-[159px] hover:text-red1 duration-200"
                >
                  Cancel
                </button>
              </div>
              <p className="font-thin text-gray-300 mt-3">Double-check information before booking.</p>
            </div>
            {/* confirm */}
            <div className="mt-10  ">
              <div>
                <button
                  onClick={handleGoBack}
                  style={{ transition: "background-color 0.2s ease-in-out, transform 0.2s ease-in-out" }}
                  className="  shadow-2xl px-[20px] py-[15px] outline-none rounded-lg bg-red1 transform hover:scale-110 duration-200 transition-transform "
                >
                  Edit Movie/Time
                </button>
                <Link to={`/Cart/${id}`}>
                  <button
                    seatsToRender={seatsToRender}
                    onClick={() => {
                      handleConfirmClick();
                    }}
                    className="  shadow-2x  px-[90px] ml-5 py-[15px] outline-none rounded-lg bg-[#1A9FFF] transform hover:scale-110 duration-200 "
                  >
                    Confrim
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default Result;
