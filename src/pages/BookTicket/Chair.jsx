import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { movieActions } from "../../store/Movie/slice";
import classNames from "classnames";
import "./style.scss";

const Chair = ({ ghe }) => {
  const dispath = useDispatch();
  const { Booking,Booked } = useSelector((state) => state.movie);

  
  return (
    <>
      <div>
        <button
        
          onClick={() => {
            dispath(movieActions.setchairBooking(ghe));
          }}
          
          className={classNames("Chair  ", {
            booking: Booking.find((e) => e.soGhe === ghe.soGhe), 
            booked: Booked.find((e)=>  e.soGhe === ghe.soGhe),
            
       
          })}
          style={{ width: "50px", width: "48px", height: "2.5rem",borderWidth: "1px", borderRadius: "0.25rem"  }}
        
        >
          {ghe?.soGhe}
        </button>
      </div>
    </>
  );
};

export default Chair;
