import React from "react";
import Chair from "./Chair";

const ChairList = ({ data }) => {
  return (
    <>
      <div>
        {data?.map((hangGhe) => {
          return (
            <div className="flex mt-3 mx-auto items-center justify-center  ">
              <p style={{ width: "40px" }} className="text-center ">
                {hangGhe.hang}
              </p>
              <div className="flex space-x-4">
                {hangGhe.danhSachGhe.map((ghe) => {
                  return <Chair ghe={ghe} />;
                })}
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default ChairList;
