import React, { useEffect, useState } from "react";
import ChairList from "./ChairList";
import Result from "./Result";
import data from "../../data/dataChair/data.json";
import { useParams } from "react-router-dom";
import { getMovies } from "../../services/QuanLyPhim";
const BookTicket = () => {

  const [movie, setMovie] = useState(null);
  const { id } = useParams();
  useEffect(() => {
    getMovies()
      .then((response) => {
        const movieData = response.data.content.find((value) => value.maPhim === parseInt(id));
        setMovie(movieData);
      })
      .catch((error) => {
        console.error("Error fetching movie data:", error);
      });
  }, [id]);
  if (!movie) {
    return <div>Loading...</div>;
  }
  return (
    <>
      <div className="mr-auto my-20 max-w-[70%] mx-auto  h-[900px] ">
        <div className="container   ">
          <div className="flex ">
            <div>
              <div className="w-full ">
                <div className=" p-8 rounded-2xl dark:bg-gray-800 dark:border-gray-700   ">
                  <h1 className="text-MaxHeader font-bold ">{movie.tenPhim}</h1>
                  <div className="text-center p-3 font-bold text-4xl bg-gray-500 text-white mt-3 rounded-lg">
                    SCREEN
                  </div>
                  <ChairList data={data} />
                </div>
              </div>
              <div className="flex justify-between mt-10 p-1 rounded-lg bg-[#2e3b4e]  ">
                <div className="px-10 py-5 flex  items-center">
                  <button className="w-12 h-10 btn border bg-[#1A9FFF]  rounded Chair"></button>
                  <h2 className="ml-5">Choosing Seats</h2>
                </div>
                <div className="px-10 py-5 flex  items-center">
                  <button className="w-12 h-10 btn border rounded Chair"></button>
                  <h2 className="ml-5">Available Seats</h2>
                </div>
                <div className="px-10 py-5 flex items-center">
                  <button className="w-12 h-10 btn border bg-[#E25050]  rounded Chair"></button>
                  <h2 className="ml-5">Occupied Seats</h2>
                </div>
              </div>
            </div>

            <div className=" ml-10 ">
              <Result data={data} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BookTicket;
