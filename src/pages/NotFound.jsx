import React from "react";
import { Link } from "react-router-dom";
import { PATH } from "../config/path";

export const NotFound = () => {
  return (
    <div className="">
      <div className="">
        <div className=" bg-[#14161d]  h-screen ">
          <div className="max-w-[70%]  mr-auto flex  justify-center mx-auto rounded-2xl ">
            <div className=" items-center mx-auto justify-center">
              <img className="w-[900px] " src="./img/img/7972494.png" alt="" />
              <div className="text-center ">
                <h2 className="text-MaxHeader text-bluebutton font-bold">404 Not Found</h2>
                <h2 className="text-white text-[60px] font-bold">Whoops! That page doesn’t exist.</h2>
                <p className="text-ContentMin text-gray-400">Here are some helpful links instead:</p>
                <div className="mt-5 text-white ">
                  <Link to="/">
                    <button className="px-6 py-3 rounded-lg bg-bluebutton mr-5 transform transition hover:scale-110 duration-200 transform-origin-center">
                      Home
                    </button>
                  </Link>
                  <button className="px-6 py-3 rounded-lg bg-bluebutton transform hover:scale-110 duration-200">
                    Contact{" "}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
