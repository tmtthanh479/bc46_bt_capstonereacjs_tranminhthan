import React, { useEffect, useState } from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // carousel
import Slider from "react-slick";
import { PATH } from "../config/path";
import { Link, useParams } from "react-router-dom";
import { getBanner, getMovies } from "../services/QuanLyPhim";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";

const Home = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
  };
  const [banner, setBanner] = useState([]);
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    getMovies()
      .then((response) => {
        setMovies(response.data.content);
      })
      .catch((error) => {
        console.error("Error fetching movie data:", error);
      });
  }, []);

  useEffect(() => {
    // Gọi hàm Axios từ tệp axiosServices.js
    getBanner()
      .then((response) => {
        setBanner(response.data.content);
      })
      .catch((error) => {
        console.error("Error fetching banner data:", error);
      });

    getMovies()
      .then((response) => {
        setMovies(response.data.content);
      })
      .catch((error) => {
        console.error("Error fetching movie data:", error);
      });
  }, []);

  return (
    <>
      {/* Banner - Carousel  */}
      <section>
        <div className=" text-center  mr-auto flex pt-20 justify-center max-w-[70%]  mx-auto ">
          <div>
            <Carousel width={900} showThumbs={false} infiniteLoop>
              {banner?.map((e) => (
                <div className="carousel-slide  ">
                  <img className="rounded-xl  shadow-2x" src={e.hinhAnh} />
                </div>
              ))}
            </Carousel>
          </div>
        </div>
      </section>

      {/* Explore More About Us*/}
      <section>
        <div className="max-w-[70%] justify-center mx-auto mt-20  ">
          <h2 className="text-left text-MaxHeader font-bold">Explore More About Us</h2>
          {/* muti-item */}

          <div>
            <div className="flex justify-between mt-10 ">
              {/* Find Theatre */}
              <Link to={PATH.theatre}>
                <div className="rounded-xl w-60 h-32 flex justify-center items-center bg-[#272B35]  transition hover:bg-[#E25050] transform hover:scale-110 duration-200   ">
                  <div className="bg-[#22262F] rounded-xl p-2 mr-6">
                    {" "}
                    <img className="w-12 h-auto " src="./img/icon/theater.png" alt="" />
                  </div>
                  <div>
                    <div className="font-medium">Find Theatre </div>
                    <p className="font-thin text-[#ada9a9]">100+ Theatre</p>
                    <button className="text-white font-thin text-ContentMin underline mt-3 ransition ">
                      View More
                    </button>
                  </div>
                </div>
              </Link>
              {/* Now Showing */}
              <Link to={PATH.AllMovie}>
                <div className="rounded-xl w-60 h-32 flex justify-center items-center bg-[#272B35] transition hover:bg-[#E25050] transform hover:scale-110 duration-200 ">
                  <div className="bg-[#22262F] rounded-xl p-2 mr-6">
                    {" "}
                    <img className="w-12 h-auto " src="./img/icon/watching-a-movie.png" alt="" />
                  </div>
                  <div>
                    <div className="font-medium  ">Now showing</div>
                    <p className="font-thin text-[#ada9a9]">20+ Movies</p>
                    <button className="text-white font-thin text-ContentMin underline mt-3">View More</button>
                  </div>
                </div>
              </Link>
              <Link to={PATH.AllMovieSoon}>
                {/* coming soon */}
                <div className="rounded-xl w-60 h-32 flex justify-center items-center bg-[#272B35] transition hover:bg-red1 transform hover:scale-110 duration-200 ">
                  <div className="bg-[#22262F] rounded-xl p-2 mr-6">
                    {" "}
                    <img className="w-12 h-auto " src="./img/icon/aboutus.png" alt="" />
                  </div>
                  <div>
                    <div className="font-medium">Coming Soon</div>
                    <p className="font-thin text-[#ada9a9]">Upcoming Movies </p>
                    <button className="text-white font-thin text-ContentMin underline mt-3">View More</button>
                  </div>
                </div>
              </Link>

              {/* News */}
              <Link to ={PATH.News}>
                <div className="rounded-xl w-60 h-32 flex justify-center items-center bg-[#272B35] transition hover:bg-[#E25050] transform hover:scale-110 duration-200 ">
                  <div className="bg-[#22262F] rounded-xl p-2 mr-6">
                    {" "}
                    <img className="w-12 h-auto " src="./img/icon/newspaper.png" alt="" />
                  </div>
                  <div>
                    <div className="font-medium">News</div>
                    <p className="font-thin text-[#ada9a9]">information</p>
                    <button className="text-white font-thin text-ContentMin underline mt-3">View More</button>
                  </div>
                </div>
              </Link>
            </div>
            {/* second item */}
            <div className="flex justify-between mt-14 ">
              {/* Food & Drink */}
              <Link to={PATH.foodAndDrink}>
                <div className="rounded-xl w-60 h-32 flex justify-center items-center bg-[#272B35] transition hover:bg-[#E25050] transform hover:scale-110 duration-200 ">
                  <div className="bg-[#22262F] rounded-xl p-2 mr-6">
                    {" "}
                    <img className="w-12 h-auto " src="./img/icon/hot-deal.png" alt="" />
                  </div>
                  <div>
                    <div className="font-medium">Food & Drink</div>
                    <p className="font-thin text-[#ada9a9]">Combo information</p>
                    <button className="text-white font-thin text-ContentMin underline mt-3">View More</button>
                  </div>
                </div>
              </Link>

              {/* Premium */}
              <div className="rounded-xl w-60 h-32 flex justify-center items-center bg-[#272B35] transition hover:bg-[#E25050] transform hover:scale-110 duration-200 ">
                <div className="bg-[#22262F] rounded-xl p-2 mr-6">
                  {" "}
                  <img className="w-12 h-auto " src="./img/icon/service.png" alt="" />
                </div>
                <div>
                  <div className="font-medium">Premium</div>
                  <p className="font-thin text-[#ada9a9]">Premium cineme</p>
                  <button className="text-white font-thin text-ContentMin underline mt-3">View More</button>
                </div>
              </div>
              {/* Ad information */}
              <div className="rounded-xl w-60 h-32 flex justify-center items-center bg-[#272B35] transition hover:bg-red1 transform hover:scale-110 duration-200 ">
                <div className="bg-[#22262F] rounded-xl p-2 mr-6">
                  {" "}
                  <img className="w-12 h-auto " src="./img/icon/megaphone.png" alt="" />
                </div>
                <div>
                  <div className="font-medium">Advertisement</div>
                  <p className="font-thin text-[#ada9a9]">Ad information</p>
                  <button className="text-white font-thin text-ContentMin underline mt-3">View More</button>
                </div>
              </div>
              {/* Our Story */}
              <div className="rounded-xl w-60 h-32 flex justify-center items-center bg-[#272B35] transition hover:bg-red1 transform hover:scale-110 duration-200 ">
                <div className="bg-[#22262F] rounded-xl p-2 mr-6">
                  {" "}
                  <img className="w-12 h-auto " src="./img/icon/aboutus.png" alt="" />
                </div>
                <div>
                  <div className="font-medium">About us</div>
                  <p className="font-thin text-[#ada9a9]">Our Story</p>
                  <button className="text-white font-thin text-ContentMin underline mt-3">View More</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* The Most Popular Moivew To Watch */}
      <section>
        <div className="max-w-[70%] justify-center  mx-auto mt-20  ">
          <div>
            <h2 className="text-left text-MaxHeader font-bold">The Most Popular Moivew To Watch </h2>
          </div>

          <div className="mt-10">
            {/*  */}

            <Tabs>
              <TabList>
                <Tab> Now Showing</Tab>
                <Tab> Upcoming Movies</Tab>
                <Tab> See more 10+</Tab>
              </TabList>

              <TabPanel>
                <div className=" justify-center mx-auto mt-10 ">
                  <div>
                    {}
                    <Slider {...settings}>
                      {movies
                        .filter((e) => e.dangChieu)
                        ?.map((e) => (
                          <div>
                            <Link to={`/MovieInfoDeital/${e.maPhim}`}>
                              <img
                                className=" w-[400px] h-[400px] p-2  rounded-2xl  shadow-2x "
                                src={e.hinhAnh}
                                alt=""
                              />
                            </Link>
                          </div>
                        ))}
                    </Slider>
                  </div>
                </div>
              </TabPanel>
              <TabPanel>
                {" "}
                <div className=" justify-center mx-auto mt-10 ">
                  <div>
                    {}
                    <Slider {...settings}>
                      {movies
                        ?.filter((e) => e.sapChieu)
                        ?.map((e) => (
                          <div>
                            <Link to={`/MovieInfoDeital/${e.maPhim}`}>
                              <img
                                className=" w-[400px] h-[400px] p-2  rounded-2xl  shadow-2x "
                                src={e.hinhAnh}
                                alt=""
                              />
                            </Link>
                          </div>
                        ))}
                    </Slider>
                  </div>
                </div>
              </TabPanel>
              <TabPanel>
                <div className=" justify-center mx-auto mt-10 ">
                  <div>
                    {}
                    <Slider {...settings}>
                      {movies?.map((e) => (
                        <div>
                          <Link to={`/MovieInfoDeital/${e.maPhim}`}>
                            <img className=" w-[400px] h-[400px] p-2  rounded-2xl  shadow-2x " src={e.hinhAnh} alt="" />
                          </Link>
                        </div>
                      ))}
                    </Slider>
                  </div>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </section>

      {/* Join and get exclusive ticket benefits*/}
      <section>
        <div className="max-w-[70%] justify-center  mx-auto my-20 ">
          <h2 className="text-center text-MaxHeader font-bold">Join and get exclusive ticket benefits</h2>
          <p className="text-center text-MinHeader text-[#ada9a9] font-thin text-sm ">
            We offer 3 membership packages. <br /> You can start with various pricing tiers and receive a multitude of
            benefits
          </p>
          <div></div>
          <div className="flex justify-between mt-10">
            {/* 1 */}
            <div className="rounded-xl w-100 text-center h-100 flex  justify-center items-center bg-[#0e0f14] transition hover:bg-[#16161d] transform hover:scale-110 duration-200">
              <div>
                <div className="mb-10">
                  <img className="w-12 h-auto mx-auto " src="./img/icon/silver.png" alt="" />
                  <p className="font-medium mt-2"> Silver Plan for those of you who like simple</p>
                  <button className="text-red1 font-thin text-MaxHeader underline mt-3">9.99$/month</button>
                </div>
                <div className="text-left items-center justify-center   ">
                  <p>
                    <span className="bg-red1  w-2 h-2 inline-block rounded-full mr-2"></span>Sneak show
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1  w-2 h-2 inline-block rounded-full mr-2"></span>Private Waiting Lounge
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1  w-2 h-2 inline-block rounded-full mr-2"></span>15% discount on combo
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1  w-2 h-2 inline-block rounded-full mr-2"></span>15% off ticket prices
                  </p>
                </div>

                <div className="mt-5">
                  <button className="mt-5 border-red1 border-2 text-red1 px-7 py-3 rounded-2xl hover:bg-[#E25050] hover:text-white duration-200">
                    Choose plan
                  </button>
                  <p className="mt-5 underline hover:text-[#C64646] cursor-pointer duration-200">About this plan</p>
                </div>
              </div>
            </div>
            {/* 2 */}
            <div className="rounded-xl w-100 text-center h-100 flex  justify-center items-center bg-[#0e0f14] transition hover:bg-[#16161d] transform hover:scale-110  duration-200">
              <div>
                <div className="mb-10">
                  <img className="w-12 h-auto mx-auto " src="./img/icon/star.png" alt="" />

                  <p className="font-medium mt-2">Gold Plan for all needs</p>
                  <button className="text-red1 font-thin text-MaxHeader underline mt-3">19.99$/month</button>
                </div>
                <div className="text-left items-center justify-center  ">
                  <p>
                    <span className="bg-red1 w-2 h-2 inline-block rounded-full mr-2"></span>Sneak show
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1 w-2 h-2 inline-block rounded-full mr-2"></span>Private Waiting Lounge
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1 w-2 h-2 inline-block rounded-full mr-2"></span>25% discount on combo
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1 w-2 h-2 inline-block rounded-full mr-2"></span>25% off ticket prices
                  </p>
                </div>

                <div className="mt-5">
                  <button className="mt-5 bg-[#C64646]  text-white px-7 py-3 rounded-2xl hover:bg-[#16161d] hover:border-[#C64646] hover:border-2 hover:text-[#C64646] duration-200">
                    Choose plan
                  </button>
                  <p className="mt-5 underline hover:text-[#C64646] cursor-pointer duration-200">About this plan</p>
                </div>
              </div>
            </div>
            {/* 3 */}
            <div className="rounded-xl w-100 text-center h-100 flex  justify-center items-center bg-[#0e0f14] transition hover:bg-[#16161d] transform hover:scale-110 duration-200 ">
              <div>
                <div className="mb-10">
                  <img className="w-12 h-auto mx-auto " src="./img/icon/diamond.png" alt="" />
                  <p className="font-medium mt-2">Diamond Plan for super deal</p>
                  <button className="text-red1 font-thin text-MaxHeader underline mt-3">29.99$/month</button>
                </div>
                <div className="text-left items-center justify-center  ">
                  <p>
                    <span className="bg-red1 w-2 h-2 inline-block rounded-full mr-2"></span>Sneak show
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1 w-2 h-2 inline-block rounded-full mr-2"></span>Private Waiting Lounge
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1 w-2 h-2 inline-block rounded-full mr-2"></span>Free 3 combo per month
                  </p>
                  <p className="mt-4">
                    <span className="bg-red1 w-2 h-2 inline-block rounded-full mr-2"></span>Free 3 tickets per month
                  </p>
                </div>

                <div className="mt-5">
                  <button className="mt-5 bg-[#C64646] border-2 text-white px-7 py-3 rounded-2xl hover:bg-[#16161d] hover:border-[#C64646] hover:border-2 hover:text-white  duration-200">
                    Choose plan
                  </button>
                  <p className="mt-5 underline hover:text-[#C64646] cursor-pointer duration-200">About this plan</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
