import React, { useEffect, useState } from "react";
import { localUserServ } from "../services/localService";
import { PATH } from "../config/path";
import { Link } from "react-router-dom";

const Userinfomation = () => {
  const [userInfo, setUserInfo] = useState([]);
 
  useEffect(() => {
    // Lấy dữ liệu từ Local Storage khi thành phần được tải
    const dataFromLocalStorage = localUserServ.get();

    if (dataFromLocalStorage) {
      // Chuyển đổi dữ liệu từ chuỗi JSON thành đối tượng JavaScript
      setUserInfo(dataFromLocalStorage);
    }
  }, []);
  const handleSignOut = () => {
    localUserServ.remove();

  };

  return (
    <div className="  max-w-[70%]  mx-auto my-20  ">
      <div className="flex justify-center mx-auto w-full bg-[#14161d]  rounded-2xl">
        <img className="w-[500px] rounded-xl " src={process.env.PUBLIC_URL + "/img/img/232.jpg"} alt="" />
        <div className=" rounded-xl w-[70%]">
          <p className=" p-5 font-bold text-MaxHeader text-center  text-red1 ">Your Account Information</p>
          {userInfo && (
            <div className="ml-5 pb-5 pr-5">
              {/* name */}
              <div className="flex items-center">
                <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                  <img className="w-[35px]" src={process.env.PUBLIC_URL + "/img/icon/User/user.png"} alt="" />
                </div>
                <div className="text-left border-gray-500   rounded p-5 border-2 m-5 w-full flex">
                  <div className="font-bold pr-10">Name:</div>
                  <div className=" ml-5 ">{userInfo.hoTen}</div>
                </div>
              </div>
              {/* Email */}
              <div className="flex items-center">
                <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                  <img className="w-[35px]" src={process.env.PUBLIC_URL + "/img/icon/User/mail.png"} alt="" />
                </div>
                <div className="text-left border-gray-500  rounded p-5 border-2 m-5 w-full flex">
                  <div className="font-bold pr-10">Email:</div>
                  <div className=" ml-5 ">{userInfo.email}</div>
                </div>
              </div>
              {/* id */}
              <div className="flex items-center">
                <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                  <img className="w-[35px]" src={process.env.PUBLIC_URL + "/img/icon/User/fingerprint.png"} alt="" />
                </div>
                <div className="text-left border-gray-500  rounded p-5 border-2 m-5 w-full flex">
                  <div className="font-bold pr-10">Name:</div>
                  <div className=" ml-5 ">{userInfo.maNhom}</div>
                </div>
              </div>
              {/* soDT */}
              <div className="flex items-center">
                <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                  <img className="w-[35px]" src={process.env.PUBLIC_URL + "/img/icon/User/telephone.png"} alt="" />
                </div>
                <div className="text-left border-gray-500  rounded p-5 border-2 m-5 w-full flex">
                  <div className="font-bold pr-10">Phone number:</div>
                  <div className=" ml-5 ">{userInfo.soDT}</div>
                </div>
              </div>
              {/* TaiKhoan */}
              <div className="flex items-center">
                <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                  <img className="w-[35px]" src={process.env.PUBLIC_URL + "/img/icon/User/information.png"} alt="" />
                </div>
                <div className="text-left border-gray-500  rounded p-5 border-2 m-5 w-full flex">
                  <div className="font-bold pr-10 border-red-300 b">User :</div>
                  <div className=" ml-5 ">{userInfo.taiKhoan}</div>
                </div>
              </div>
              <div className="text-right px-5  ">
                {" "}
                <Link onClick={handleSignOut} to="/">
                  <button className="hover:text-red1 duration-200">Sign out</button>
                </Link>
              </div>
            </div>
          )}
        </div>
      </div>
      <Link to={PATH.myticket}>
        <div className="text-MaxHeader text-white font-bold bg-red1 hover:bg-red-800 duration-200 rounded-xl p-5 mt-10 cursor-pointer text-center ">
          Your Ticket
        </div>
      </Link>
    </div>
  );
};

export default Userinfomation;
