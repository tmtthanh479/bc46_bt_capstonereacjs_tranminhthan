import React, { useEffect, useState } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useDispatch } from "react-redux";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css"; // Import CSS của react-tabs
import {
  getMovies,
  getRap,
  getlocationBHD,
  getlocationCGV,
  getlocationCineGalaxy,
  getlocationCineLotteCinima,
  getlocationCineMegaGS,
  getlocationCineStar,
  gettimecBHD,
  gettimecgv,
} from "../../services/QuanLyPhim";
import { Link, useParams } from "react-router-dom";
import { movieActions } from "../../store/Movie/slice";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const MovieInfoDeitalCenema = () => {
  const [activeTab, setActiveTab] = useState(null);

  const handleTabClick = (tabIndex) => {
    setActiveTab(tabIndex);
  };

  //
  const [movie, setMovie] = useState(null);
  const { id } = useParams();
  const dispath = useDispatch();
  // Call api Cinema - Location - Showtime
  const [rap, setRap] = useState([]);
  const [timeBHD, setTimeBHD] = useState([]);
  const [locationBHD, setLocationBHD] = useState();
  const [locationCGV, setLocationCGV] = useState();
  const [locationGalaxy, setLocationGalaxy] = useState();
  const [locationLotteCinima, setLocationLotteCinima] = useState();
  const [locationMegaGS, setLocationMegaGS] = useState();
  const [locationCineStar, setLocationCineStar] = useState();
  useEffect(() => {
    getRap()
      .then((response) => {
        setRap(response.data.content);
      })
      .catch((error) => {
        console.error("Error fetching banner data:", error);
      });
    getlocationBHD()
      .then((res) => {
        setLocationBHD(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getlocationCGV()
      .then((res) => {
        setLocationCGV(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getlocationCineStar()
      .then((res) => {
        setLocationCineStar(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getlocationCineGalaxy()
      .then((res) => {
        setLocationGalaxy(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getlocationCineLotteCinima()
      .then((res) => {
        setLocationLotteCinima(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getlocationCineMegaGS()
      .then((res) => {
        setLocationMegaGS(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    gettimecBHD()
      .then((res) => {
        // Khởi tạo một mảng để lưu trữ giá trị ngayChieuGioChieu
        const ngayChieuGioChieuList = [];

        // Duyệt qua tất cả các phần tử trong dữ liệu
        res.data.content.forEach((item) => {
          item.lstCumRap.forEach((cumRapItem) => {
            cumRapItem.danhSachPhim.forEach((phimItem) => {
              phimItem.lstLichChieuTheoPhim.forEach((lichChieuItem) => {
                // Lấy giá trị ngayChieuGioChieu và thêm vào mảng
                ngayChieuGioChieuList.push(lichChieuItem.ngayChieuGioChieu);
              });
            });
          });
        });

        // Lấy 7 giá trị đầu tiên
        const firstSevenNgayChieuGioChieu = ngayChieuGioChieuList.slice(0, 7);

        // Gán mảng 7 giá trị ngayChieuGioChieu cho setTimeBHD
        setTimeBHD(firstSevenNgayChieuGioChieu);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  //   call info film
  useEffect(() => {
    getMovies()
      .then((response) => {
        const movieData = response.data.content.find((value) => value.maPhim === parseInt(id));
        setMovie(movieData);
      })
      .catch((error) => {
        console.error("Error fetching movie data:", error);
      });
  }, [id]);

  //   toast & bắt user chọn
  useEffect(() => {
    toast.info("Visit Food & Drinks for an enhanced experience.", {
      position: "top-right",
      autoClose: 8000,
      style: {
        background: "#14161d",
        color: "white",
      },
    });
  }, []);

  const handleConfirmClick = () => {
    if (activeTab !== null) {
      console.log("Confirmed");
    } else {
      toast.error("Please select Cinema, Location, and Showtime.", {
        position: "top-right",
        autoClose: 5000,
        style: {
          background: "#14161d",
          color: "white",
        },
      });
    }
  };

  return (
    <>
      <div className="max-w-[70%] justify-center  mx-auto my-20   rounded-xl p-5 ">
        <div className="text-MaxHeader font-bold bg-[#272B35] text-red1 rounded-xl p-5 ">
          Select Cinema - Location - Showtime
        </div>

        {/* info film */}
        <section>
          <div className="flex mt-10">
            <div>
              <img className="rounded-xl h-[600px]" src={movie?.hinhAnh} alt="" />
            </div>
            <div className=" bg-[#14161d] rounded-xl p-5 ml-10  w-[1100px]">
              <h2 className="text-MaxHeader font-bold ">{movie?.tenPhim}</h2>
              <h2 class="font-medium mt-10">
                Release date:<span class="font-thin"> {movie?.ngayKhoiChieu}</span>
              </h2>
              <h2 class="font-medium mt-10">
                Movie code:<span class="font-thin"> {movie?.maPhim}</span>
              </h2>
              <h2 class="font-medium mt-10">
                Language:<span class="font-thin"> Vietnamese Subtitles,Vietnamese Dubbed</span>
              </h2>
              <h2 class="font-medium mt-10">
                Rate:<span class="font-normal"> {movie?.danhGia}/10</span>
              </h2>
              <h2 class="font-medium mt-10">
                Movie Synopsis:
                <div className="bg-[#14161d]  font-thin rounded-xl p-5 mt-10 ">{movie?.moTa}</div>
              </h2>
            </div>
            {/*  */}
          </div>
        </section>

        {/* Function chọn Cinema - Location -Showtime  */}
        <section>
          <div className=" font-bold bg-[#14161d] rounded-xl mt-10 p-5 ">
            <div>
              <Tabs defaultIndex={-1}>
                {/* rap */}
                <div className=" py-5 my-5">
                  <TabList className="justify-between flex">
                    {rap?.map((e) => (
                      <Tab key={e?.maHeThongRap}>
                        <img
                          onClick={() => {
                            dispath(movieActions.setBookinfoFilmRap(e));
                          }}
                          className="w-36"
                          src={e.logo}
                          alt=""
                        />
                      </Tab>
                    ))}
                  </TabList>
                </div>

                {/* BHD */}
                <TabPanel>
                  {/* defaultIndex={-1} bỏ chọn tab mặc định */}
                  <Tabs defaultIndex={-1}>
                    <TabList className=" m-5 p-5 border-1 rounded-xl border-[#272B35] ">
                      {locationBHD?.map((e) => (
                        <Tab key={e?.id}>
                          <button
                            onClick={() => {
                              dispath(movieActions.setBookinfoFilmLoacation(e));
                              handleTabClick(0);
                            }}
                            className="p-2 "
                          >
                            {e?.tenCumRap}
                          </button>
                        </Tab>
                      ))}
                    </TabList>

                    <div className=" m-5 p-5 border-1 rounded-xl border-[#272B35] ">
                      {timeBHD?.map((ngayChieuGioChieu, maLichChieu) => (
                        <button
                          onClick={() => {
                            dispath(movieActions.setBookinfoFilmTime(ngayChieuGioChieu));
                            handleTabClick(2);
                          }}
                          className="p-3 m-2 bg-[#272B35] rounded  focus:bg-white focus:text-black"
                          key={maLichChieu}
                        >
                          {ngayChieuGioChieu}
                        </button>
                      ))}
                    </div>
                  </Tabs>
                </TabPanel>

                {/* cgv */}
                <TabPanel>
                  <Tabs defaultIndex={-1}>
                    <TabList className=" m-5 p-5 border-1 rounded-xl border-[#272B35] ">
                      {locationCGV?.map((e) => (
                        <Tab key={e?.id}>
                          <button
                            onClick={() => {
                              dispath(movieActions.setBookinfoFilmLoacation(e));
                              handleTabClick(3);
                            }}
                            className="p-2"
                          >
                            {e?.tenCumRap}
                          </button>
                        </Tab>
                      ))}
                    </TabList>

                    <div className="m-5 p-5 border-1 rounded-xl border-[#272B35]">
                      {timeBHD?.map((ngayChieuGioChieu, maLichChieu) => (
                        <button
                          onClick={() => {
                            dispath(movieActions.setBookinfoFilmTime(ngayChieuGioChieu));
                            handleTabClick(4);
                          }}
                          className="p-3 m-2 bg-[#272B35] rounded  focus:bg-white focus:text-black"
                          key={maLichChieu}
                        >
                          {ngayChieuGioChieu}
                        </button>
                      ))}
                    </div>
                  </Tabs>
                </TabPanel>

                {/*CineStar */}
                <TabPanel>
                  <Tabs defaultIndex={-1}>
                    <TabList className=" m-5 p-5 border-1 rounded-xl border-[#272B35] ">
                      {locationCineStar?.map((e) => (
                        <Tab key={e?.id}>
                          <button
                            onClick={() => {
                              dispath(movieActions.setBookinfoFilmLoacation(e));
                              handleTabClick(5);
                            }}
                            className="p-2"
                          >
                            {e?.tenCumRap}
                          </button>
                        </Tab>
                      ))}
                    </TabList>

                    <div className="m-5 p-5 border-1 rounded-xl border-[#272B35]">
                      {timeBHD?.map((ngayChieuGioChieu, maLichChieu) => (
                        <button
                          onClick={() => {
                            dispath(movieActions.setBookinfoFilmTime(ngayChieuGioChieu));
                            handleTabClick(6);
                          }}
                          className="p-3 m-2 bg-[#272B35] rounded  focus:bg-white focus:text-black"
                          key={maLichChieu}
                        >
                          {ngayChieuGioChieu}
                        </button>
                      ))}
                    </div>
                  </Tabs>
                </TabPanel>

                {/* galaxy */}
                <TabPanel>
                  <Tabs defaultIndex={-1}>
                    <TabList className=" m-5 p-5 border-1 rounded-xl border-[#272B35] ">
                      {locationGalaxy?.map((e) => (
                        <Tab key={e?.id}>
                          <button
                            onClick={() => {
                              dispath(movieActions.setBookinfoFilmLoacation(e));
                              handleTabClick(7);
                            }}
                            className="p-2"
                          >
                            {e?.tenCumRap}
                          </button>
                        </Tab>
                      ))}
                    </TabList>

                    <div className="m-5 p-5 border-1 rounded-xl border-[#272B35]">
                      {timeBHD?.map((ngayChieuGioChieu, maLichChieu) => (
                        <button
                          onClick={() => {
                            dispath(movieActions.setBookinfoFilmTime(ngayChieuGioChieu));
                            handleTabClick(9);
                          }}
                          className="p-3 m-2 bg-[#272B35] rounded  focus:bg-white focus:text-black"
                          key={maLichChieu}
                        >
                          {ngayChieuGioChieu}
                        </button>
                      ))}
                    </div>
                  </Tabs>
                </TabPanel>

                {/* LotteCinima */}
                <TabPanel>
                  <Tabs defaultIndex={-1}>
                    <TabList className=" m-5 p-5 border-1 rounded-xl border-[#272B35] ">
                      {locationLotteCinima?.map((e) => (
                        <Tab key={e?.id}>
                          <button
                            onClick={() => {
                              dispath(movieActions.setBookinfoFilmLoacation(e));
                              handleTabClick(9);
                            }}
                            className="p-2"
                          >
                            {e?.tenCumRap}
                          </button>
                        </Tab>
                      ))}
                    </TabList>

                    <div className="m-5 p-5 border-1 rounded-xl border-[#272B35]">
                      {timeBHD?.map((ngayChieuGioChieu, maLichChieu) => (
                        <button
                          onClick={() => {
                            dispath(movieActions.setBookinfoFilmTime(ngayChieuGioChieu));
                            handleTabClick(10);
                          }}
                          className="p-3 m-2 bg-[#272B35] rounded  focus:bg-white focus:text-black"
                          key={maLichChieu}
                        >
                          {ngayChieuGioChieu}
                        </button>
                      ))}
                    </div>
                  </Tabs>
                </TabPanel>
                {/* MegaGS */}
                <TabPanel>
                  <Tabs defaultIndex={-1}>
                    <TabList className=" m-5 p-5 border-1 rounded-xl border-[#272B35] ">
                      {locationMegaGS?.map((e) => (
                        <Tab key={e.id}>
                          <button
                            onClick={() => {
                              dispath(movieActions.setBookinfoFilmLoacation(e));
                              handleTabClick(11);
                            }}
                            className="p-2"
                          >
                            {e.tenCumRap}
                          </button>
                        </Tab>
                      ))}
                    </TabList>

                    <div className="m-5 p-5 border-1 rounded-xl border-[#272B35]">
                      {timeBHD?.map((ngayChieuGioChieu, maLichChieu) => (
                        <button
                          onClick={() => {
                            dispath(movieActions.setBookinfoFilmTime(ngayChieuGioChieu));
                            handleTabClick(12);
                          }}
                          className="p-3 m-2 bg-[#272B35] rounded  focus:bg-white focus:text-black"
                          key={maLichChieu}
                        >
                          {ngayChieuGioChieu}
                        </button>
                      ))}
                    </div>
                  </Tabs>
                </TabPanel>
              </Tabs>
            </div>
          </div>
        </section>

        <Link to={activeTab !== null ? `/BookTicket/${id}` : null}>
          <div className="  bg-red1 rounded-xl p-5 mt-10 cursor-pointer  hover:bg-red-800   ">
            <div onClick={handleConfirmClick} className="text-center text-MinHeader font-bold  ">
              Confirm Booking Details
            </div>
          </div>
        </Link>
      </div>
    </>
  );
};

export default MovieInfoDeitalCenema;
