import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { getMovies } from "../../services/QuanLyPhim";

const MovieInfoDeital = () => {
  const [movie, setMovie] = useState(null);
  const { id } = useParams();
  useEffect(() => {
    getMovies()
      .then((response) => {
        const movieData = response.data.content.find((value) => value.maPhim === parseInt(id));
        setMovie(movieData);
      })
      .catch((error) => {
        console.error("Error fetching movie data:", error);
      });
  }, [id]);

  return (
    <>
      <div className="max-w-[70%] justify-center  mx-auto my-20  rounded-xl p-5 ">
        <div className="text-MaxHeader font-bold bg-[#14161d] rounded-xl p-5 text-red1 ">Film Information</div>
        {/*  */}
        <div className="flex justify-between mt-10">
          <div>
            <img className="rounded-xl w-[600px]" src={movie?.hinhAnh} alt="" />
          </div>
          <div className=" bg-[#14161d] rounded-xl p-5 ml-10 w-[800px]">
            <h2 className="text-MaxHeader font-bold ">{movie?.tenPhim}</h2>
            <h2 class="font-medium mt-10">
              Release date:<span class="font-thin"> {movie?.ngayKhoiChieu}</span>
            </h2>
            <h2 class="font-medium mt-10">
              Movie code:<span class="font-thin"> {movie?.maPhim}</span>
            </h2>
            <h2 class="font-medium mt-10">
              Language:<span class="font-thin"> Vietnamese Subtitles,Vietnamese Dubbed</span>
            </h2>
            <h2 class="font-medium mt-10">
              Rate:<span class="font-thin"> {movie?.danhGia}/10</span>
            </h2>
            <h2 class="font-medium mt-10">
              Movie Synopsis:
              <div className="bg-[#14161d]  font-thin rounded-xl p-5 mt-10 ">{movie?.moTa}</div>
            </h2>
            <Link to={`/MovieInfoDeitalCenema/${id}`}>
              {movie?.dangChieu ? (
                <button className="mt-10 bg-[#C64646] text-white px-7 py-3 rounded-lg hover:bg-red-800 duration-200">
                  Book tickets
                </button>
              ) : null}
            </Link>
          </div>
          {/*  */}
        </div>
      </div>
    </>
  );
};

export default MovieInfoDeital;
