import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { getMovies } from "../services/QuanLyPhim";
import { useDispatch, useSelector } from "react-redux";
import { movieActions } from "../store/Movie/slice";

const Cart = () => {
  const [movie, setMovie] = useState(null);
  const dispath = useDispatch();

  const latestInforFilmRap = useSelector((state) => state.movie.InforFilmRap[state.movie.InforFilmRap.length - 1]);
  const latestInforFilmTime = useSelector(
    (state) => state.movie.setBookinfoFilmTime[state.movie.setBookinfoFilmTime.length - 1]
  );
  const latestInforLocation = useSelector(
    (state) => state.movie.InforFilmLocation[state.movie.InforFilmLocation.length - 1]
  );
  const Booking = useSelector((state) => state.movie.Booking);
  const seatsToRender = Booking.length === 0 ? [{}] : Booking;

  const { id } = useParams();
  useEffect(() => {
    getMovies()
      .then((response) => {
        const movieData = response.data.content.find((value) => value.maPhim === parseInt(id));
        setMovie(movieData);
        localStorage.setItem("selectedMovie", JSON.stringify(movieData));
        localStorage.setItem("latestInforFilmRap", JSON.stringify(latestInforFilmRap));
        localStorage.setItem("latestInforFilmTime", JSON.stringify(latestInforFilmTime));
        localStorage.setItem("latestInforLocation", JSON.stringify(latestInforLocation));
      })
      .catch((error) => {
        console.error("Error fetching movie data:", error);
      });
  }, [id]);

  if (!movie) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <div className="my-20 ">
        <div className="text-MaxHeader mb-10 font-bold bg-[#272B35] text-red1 rounded-xl p-5 max-w-[70%]  flex  mx-auto ">
          Cart
        </div>
        <div className="text-center  mr-auto flex rounded-lg justify-center max-w-[70%] mx-auto bg-[#14161d] p-20   ">
          <div className="flex ">
            <div className="w-1/2   justify-center flex items-center mr-auto ">
              <img className="w-[500px] mr-20" src={process.env.PUBLIC_URL + "/img/img/Kerfin7_NEA_2579.png"} alt="" />
            </div>

            <div className=" w-1/2 p-5    ">
              {/* movie title */}
              <div className=" py-5">
                <div className="flex items-center ">
                  <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                    <img
                      className="h-[32px] w-[32px]"
                      src={process.env.PUBLIC_URL + "/img/icon/Cart/movie-camera.png"}
                      alt=""
                    />
                  </div>
                  <div className=" p-6 flex rounded-lg bg-[#2e3b4e] w-full ">
                    <p className=" font-bold">Movie:</p>
                    <h2 className=" ml-[38px] font-thin">{movie.tenPhim}</h2>
                  </div>
                </div>
              </div>

              <div>
                {/* Địa điểm */}
                <div className="py-5">
                  <div className="flex items-center ">
                    <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                      <img
                        className="h-[32px] w-[32px]"
                        src={process.env.PUBLIC_URL + "/img/icon/Cart/3d-glasses.png"}
                        alt=""
                      />
                    </div>
                    <div className=" p-6 flex rounded-lg  bg-[#2e3b4e] w-full  overflow-x-auto">
                      <p className=" h-6 font-bold">Location:</p>
                      <h2 className=" h-6 ml-5  flex">{latestInforLocation?.diaChi} </h2>
                    </div>
                  </div>
                </div>
                {/* loại rạp */}
                <div className="py-5">
                  <div className="flex items-center ">
                    <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                      <img
                        className="h-[32px] w-[32px]"
                        src={process.env.PUBLIC_URL + "/img/icon/Cart/ticket.png"}
                        alt=""
                      />
                    </div>
                    <div className=" p-6 flex rounded-lg bg-[#2e3b4e] w-full ">
                      <p className="pr-6 font-bold">Cinema:</p>
                      <h2 className=" ml-[7px]">{latestInforFilmRap?.tenHeThongRap}</h2>
                    </div>
                  </div>
                </div>
                {/* thời gian */}
                <div className="py-5">
                  <div className="flex items-center ">
                    <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                      <img
                        className="h-[32px] w-[32px]"
                        src={process.env.PUBLIC_URL + "/img/icon/Cart/clapperboard.png"}
                        alt=""
                      />
                    </div>
                    <div className=" p-6 flex rounded-lg bg-[#2e3b4e] w-full ">
                      <p className="font-bold">Time:</p>
                      <h2 className=" ml-[48px]">{latestInforFilmTime}</h2>
                    </div>
                  </div>
                </div>
                {/* ghế */}
                <div className="py-5">
                  <div className="flex items-center ">
                    <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                      <img
                        className="h-[32px] w-[32px]"
                        src={process.env.PUBLIC_URL + "/img/icon/BookTicketIcon/armchair.png"}
                        alt=""
                      />
                    </div>

                    <div className=" p-6 flex rounded-lg bg-[#2e3b4e] w-full overflow-x-auto ">
                      <p className="h-6 font-bold">Seats:</p>
                      {seatsToRender.map((e) => (
                        <h2 className=" h-6 ml-[46px]  flex">{e.soGhe}</h2>
                      ))}
                    </div>
                  </div>
                </div>
                {/* combo */}
                <div className="py-5">
                  <div className="flex items-center ">
                    <div className="rounded-lg mr-2 bg-[#2e3b4e] p-5">
                      <img
                        className="h-[32px] w-[32px]"
                        src={process.env.PUBLIC_URL + "/img/icon/BookTicketIcon/armchair.png"}
                        alt=""
                      />
                    </div>

                    <div className=" p-6 flex rounded-lg bg-[#2e3b4e] w-full ">
                      <p className="font-bold">Combo:</p>
                      <h2 className=" ml-[33px]">dsadsa</h2>
                    </div>
                  </div>
                </div>
                {/* check out */}
                <div className="rounded-lg w-full  bg-[#2e3b4e] p-5 ">
                  <div className="flex p-2  ">
                    <p className="text-left font-bold">Payment</p>
                  </div>
                  <div className="flex p-2  justify-between">
                    <p className="text-left font-bold">Your seat</p>
                    <p className=" font-bold">Per Ticket Price</p>
                  </div>
                  {seatsToRender.map((e) => (
                    <div className=" p-2  ">
                      <div className="justify-between flex">
                        <p className="text-left font-thin">{e.soGhe}</p>
                        <p className="font-thin">{e.gia}</p>
                      </div>
                    </div>
                  ))}

                  <div className="flex p-2  justify-between">
                    <p className="text-left font-bold">Combo</p>
                    <p className="font-bold">Per Combo Price</p>
                  </div>
                  <div className="flex p-2  justify-between">
                    <p className="text-left font-bold text-MaxHeader">
                      {" "}
                      {Booking.reduce((total, ghe) => (total += ghe.gia), 0).toLocaleString("vi-VN", {
                        style: "currency",
                        currency: "VND",
                      })}
                    </p>
                    <Link to = {`/DatVeSuccess/${id}`}>
                      <button
                        onClick={() => {
                          dispath(movieActions.setChairBooked(1));
                        }}
                        className="border-2 px-[90px]   py-[15px] outline-none rounded-lg bg-[#1A9FFF] transform hover:scale-110 duration-200"
                      >
                        Check out
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
