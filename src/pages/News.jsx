import React from "react";

const News = () => {
  return (
    <>
      <div className="max-w-[70%] justify-center  mx-auto my-20  rounded-xl p-5 ">
        <div className="text-MaxHeader font-bold bg-[#14161d] rounded-xl p-5 text-red1 ">News</div>
        <div>
          <section className="mt-10 ">
            <div className="rounded-lg   ">
              <h2 className="text-MinHeader font-bold rounded-xl pl-5 pt-5 ">Today's Top Stories</h2>
              <div className="flex justify-between   rounded-lg  mt-5  ">
                {/* item */}
                <button className="w-1/4 p-4  transform hover:scale-110 duration-200 ">
                  <div className="bg-[#14161d]  rounded-lg shadow-md">
                    <img
                      src="https://media.lottecinemavn.com/Media/Event/7516fd5711b241e0b5e3ab7024764604.jpg"
                      alt="Image 1"
                      className="w-full h-48 object-cover rounded-t-lg"
                    />
                    <h2 className="text-xl font-semibold p-4 h-28 mt-2 rounded-lg bg-[#14161d] ">
                      Cosplay or Wear Traditional Attire for Exclusive Rewards!
                    </h2>
                  </div>
                </button>

                <button className="w-1/4 p-4 transform hover:scale-110 duration-200  ">
                  <div className="bg-[#14161d]  rounded-lg shadow-md">
                    <img
                      src="https://media.lottecinemavn.com/Media/Event/69d050a4c01b4081afdf23bb323823dd.png"
                      alt="Image 1"
                      className="w-full h-48 object-cover rounded-t-lg"
                    />
                    <h2 className="text-xl font-semibold p-4  rounded-lg bg-[#14161d] h-28 mt-2 ">
                      Use Cinema Coin for Payments and Rewards
                    </h2>
                  </div>
                </button>

                <button className="w-1/4 p-4  transform hover:scale-110 duration-200 ">
                  <div className="bg-[#14161d]  rounded-lg shadow-md">
                    <img
                      src="https://media.lottecinemavn.com/Media/Event/b874751c03c946e5aa2afab55ccd101d.png"
                      alt="Image 1"
                      className="w-full h-48 object-cover rounded-t-lg"
                    />
                    <h2 className="text-xl font-semibold p-4  rounded-lg bg-[#14161d] h-28 mt-2 ">
                      Join Our Membership for Jaw-Dropping Weekly Deals at the Cinema!
                    </h2>
                  </div>
                </button>

                <button className="w-1/4 p-4 transform hover:scale-110 duration-200  ">
                  <div className="bg-[#14161d]  rounded-lg shadow-md">
                    <img
                      src="https://media.lottecinemavn.com/Media/Event/11302f2fd63f40ccb9362ac0f1381450.png"
                      alt="Image 1"
                      className="w-full h-48 object-cover rounded-t-lg"
                    />
                    <h2 className="text-xl font-semibold p-4  rounded-lg bg-[#14161d] h-28 mt-2 ">
                      U22 Member Flat-Rate Ticket Program
                    </h2>
                  </div>
                </button>
              </div>
            </div>
          </section>

          <section className="mt-10">
            {/* item */}
            <div className="mb-10 border-b-1 border-gray-800 cursor-pointer">
              <div className=" flex rounded-xl ">
                <div className="p-2 flex">
                  <img
                    className="rounded-xl"
                    src="https://www.cgv.vn/media/wysiwyg/2023/092023/240x201-px-CGV.jpg"
                    alt=""
                  />
                  <div className="ml-5 flex items-center  w-[800px]">
                    <div>
                      <h2 className="text-MinHeader">Love Popcorn and Soda? Collect Points for Instant Rewards!</h2>
                      <p className="text-gray-300">
                        "Top 100 CGV Members with the Highest Concession and Merchandise Spending Each Week - Each
                        Prize: Receive 01 Fashionable Bag. Minimum weekly spending requirement of 300,000 VND."
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="mb-10 border-b-1 border-gray-800 cursor-pointer ">
              <div className=" flex rounded-xl ">
                <div className="p-2 flex">
                  <img
                    className="rounded-xl "
                    src="https://www.cgv.vn/media/wysiwyg/2023/082023/The_Nun_GGG_240x201.jpg "
                    alt=""
                  />
                  <div className="ml-5 flex items-center   w-[800px]">
                    <div>
                      <h2 className="text-MinHeader">
                        Experience a Spine-Chilling Encounter with Miss Valak with High-Definition Audio{" "}
                      </h2>
                      <p className="text-gray-300   ">
                        "From September 1, 2023, until supplies last: The first 3,300 customers who book tickets to see
                        'The Conjuring 2' in IMAX format will receive a 30,000 VND Golden Gate voucher."
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="mb-10 border-b-1 border-gray-800 cursor-pointer ">
              <div className=" flex rounded-xl ">
                <div className="p-2 flex">
                  <img
                    className="rounded-xl "
                    src="https://www.cgv.vn/media/wysiwyg/2023/082023/CGV-Banner_240X201.png "
                    alt=""
                  />
                  <div className="ml-5 flex items-center   w-[800px]">
                    <div>
                      <h2 className="text-MinHeader">COSY X CGV Joining Hearts, Discover Now</h2>
                      <p className="text-gray-300   ">
                        Free Cosy Popping Cake 'DRINK-OUT' with Popcorn & Drink Combo at CGV!
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="mb-10 border-b-1 border-gray-800 cursor-pointer">
              <div className=" flex rounded-xl ">
                <div className="p-2 flex">
                  <img
                    className="rounded-xl "
                    src="https://www.cgv.vn/media/wysiwyg/2023/072023/240x201_2.jpg "
                    alt=""
                  />
                  <div className="ml-5 flex items-center   w-[800px]">
                    <div>
                      <h2 className="text-MinHeader">
                        Special Challenge Program - Elevate Your Experience for U22 Members
                      </h2>
                      <p className="text-gray-300   ">
                        "Gift 100 points to the first 999 U22 members who EXPERIENCE ALL 03 OUT OF 05 SPECIAL-FORMAT
                        SCREENS at CGV: STARIUM – PREMIUM."
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="mb-10 border-b-1 border-gray-800 cursor-pointer ">
              <div className=" flex rounded-xl ">
                <div className="p-2 flex">
                  <img
                    className="rounded-xl "
                    src="https://www.cgv.vn/media/wysiwyg/2023/072023/240x201_1.jpg "
                    alt=""
                  />
                  <div className="ml-5 flex items-center   w-[800px]">
                    <div>
                      <h2 className="text-MinHeader">
                        CGV Gift Card & CGV eGift: Sweet Gifts, Endless Movie Enjoyment
                      </h2>
                      <p className="text-gray-300   ">
                      "Are you looking for a gift to express gratitude and appreciation to loved ones, friends, colleagues, or partners?"
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="mb-10 border-b-1 border-gray-800 cursor-pointer">
              <div className=" flex rounded-xl ">
                <div className="p-2 flex">
                  <img
                    className="rounded-xl "
                    src="https://www.cgv.vn/media/wysiwyg/2023/012023/240x201.jpg "
                    alt=""
                  />
                  <div className="ml-5 flex items-center   w-[800px]">
                    <div>
                      <h2 className="text-MinHeader">
                      CGV 2023 Member Benefits Policy
                      </h2>
                      <p className="text-gray-300   ">
                      "CJ CGV Vietnam sincerely thanks all our customers for accompanying us on the cinematic journey throughout the past year, 2022."
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </>
  );
};

export default News;
