import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

const Register = () => {
  const [movieData, setMovieData] = useState(null);
  const [latestInforFilmRap, setLatestInforFilmRap] = useState(null);
  const [latestInforFilmTime, setLatestInforFilmTime] = useState(null);
  const [latestInforLocation, setLatestInforLocation] = useState(null);
  const Booked = useSelector((state) => state.movie.Booked);
  console.log("🚀 ~ file: Myticket.jsx:10 ~ Register ~ Booked:", Booked)

  useEffect(() => {
    // Lấy dữ liệu từ Local Storage khi component được tạo
    const storedMovieData = localStorage.getItem("selectedMovie");
    const storedInforFilmRap = localStorage.getItem("latestInforFilmRap");
    const storedInforFilmTime = localStorage.getItem("latestInforFilmTime");
    const storedInforLocation = localStorage.getItem("latestInforLocation");

    // Kiểm tra nếu có dữ liệu trong Local Storage
    if (storedMovieData) {
      // Chuyển đổi dữ liệu từ chuỗi JSON thành đối tượng JavaScript
      const parsedMovieData = JSON.parse(storedMovieData);
      setMovieData(parsedMovieData);
    }

    if (storedInforFilmRap) {
      const parsedInforFilmRap = JSON.parse(storedInforFilmRap);
      setLatestInforFilmRap(parsedInforFilmRap);
    }

    if (storedInforFilmTime) {
      const parsedInforFilmTime = JSON.parse(storedInforFilmTime);
      setLatestInforFilmTime(parsedInforFilmTime);
    }

    if (storedInforLocation) {
      const parsedInforLocation = JSON.parse(storedInforLocation);
      setLatestInforLocation(parsedInforLocation);
    }
  }, []);
  return (
    <>
      <div className="bg-[#14161d] rounded-2xl max-w-[70%] justify-center flex mx-auto my-20"></div>
      <div className="bg-[#14161d] rounded-2xl max-w-[70%] justify-center flex  p-10  mx-auto   my-20">
        <div className="  ">
          <div>
            <div className="flex p-5">
              <div className="w-70/100 p-10 border-gray-400 border-dashed  border-2 w-[800px] rounded-2xl shadow-sm ">
                <p className="text-center mr-36">{latestInforFilmRap?.tenHeThongRap}</p>
                <h2 className="text-[80px] font-bold text-center">{movieData?.tenPhim}</h2>
                <p className="text-center">{latestInforLocation?.diaChi}</p>
                <div className="justify-between flex">
               
                  <p>{movieData?.ngayKhoiChieu}</p>
                  {Booked.map((e)=>
                 <p>{ e.soGhe}</p>
                  )}
                </div>
              </div>
              <div className="w-30/100 p-5 border-gray-400 border-dashed  border-y-2 border-r-2 w-[300px]  rounded-2xl ">
                <p className="flex">3232</p>
                <p className="transform rotate-90 flex   text-[25px] font-bold mr-auto mt-[104px] justify-center mx-auto ">
                  500.000.000đ
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Register;
