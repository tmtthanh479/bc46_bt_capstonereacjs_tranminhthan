import React, { useEffect, useState } from "react";
import {
  validateEmail,
  validateEqualMk,
  validateHt,
  validateMk,
  validatePhoneVietNam,
  validateTk,
} from "./ValidationForm";
import axios from "axios";
import { Button, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { userServ } from "../services/QuanLyNguoiDung";
import { closeFormSuccess, registerSuccess } from "../store/Movie/formSuccessSlice";
import { localUserServ } from "../services/localService";
import SuccessForm from "../components/layouts/SuccessForm";
import { PATH } from "../config/path";
import { getregister } from "../services/login&register";

const Register = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  // validate
  const onFinish = async (values) => {
    let cloneValues = { ...values, maNhom: "GP09" };
    let { taiKhoan, matKhau, hoTen, email, soDt } = cloneValues;
    let fetchRegisterUser = async () => {
      let isValid =
        validateTk(taiKhoan) &&
        validateMk(matKhau) &&
        validateHt(hoTen) &&
        validateEmail(email) &&
        validatePhoneVietNam(soDt);
      if (isValid) {
        try {
          let response = await userServ.registerUser(values);
          console.log(response);
          dispatch(registerSuccess());
          localUserServ.set(response.data.content);
          // * sau 2s sẽ tắt thông báo
          setTimeout(() => {
            dispatch(closeFormSuccess());
          }, 2000);
          // * sau 3s sẽ chuyển tới trang đăng nhập
          setTimeout(() => {
            navigate("/login");
          }, 3000);
        } catch (err) {
          console.log(err);
          message.error(err.response.data.content);
        }
      }
    };
    fetchRegisterUser();
  };
  // failed
  const onFinishFailed = (errInfo) => {
    console.log("Failed:", errInfo);
  };
  //
  const [register, setRegister] = useState([]);
  useEffect(() => {
    getregister()
      .then((response) => {
        setRegister(response.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  });
  return (
    <>
      <SuccessForm />
      <div className=" max-w-[70%] mr-auto flex my-20 justify-center mx-auto rounded-2xl bg-[#14161d]  ">
        <div className="w-1/2 flex items-center justify-center ">
          <img className="w-[500px] " src="./img/img/Kerfin7_NEA_2579.png" alt="" />
        </div>
        <div className="w-1/2 ">
          <Form
            name="basic"
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
            className="max-w-[70%] rounded-lg"
            style={{
              background: "white",
              margin: "auto",
              backgroundColor: "#14161d",
              marginTop: "80px",
              marginBottom: "80px",
              width: "full",
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <div className="w-full mb-10 space-y-5 text-center ">
              <i style={{ lineHeight: "45px", borderRadius: "50%", text: "white" }}></i>
              <h2 className="text-3xl font-bold text-white text-left ">Welcome to the cinema!</h2>
              <p className=" font-thin text-[#ada9a9] text-left">Please sign up for the best experience</p>
            </div>
            <div className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white"> User</div>
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Nhập tài khoản",
                },
              ]}
            >
              <Input
                className=" focus:border-white  text-gray-900 text-sm rounded-lg  p-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white "
                placeholder="nhập tài khoản ..."
              />
            </Form.Item>
            <div className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your password</div>
            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: " mật khẩu",
                },
              ]}
            >
              <Input
                type="password"
                className="focus:border-white  text-gray-900 text-sm rounded-lg  p-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white"
                placeholder="mật khẩu ..."
              />
            </Form.Item>
            <div className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white">Name</div>
            <Form.Item
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Nhập Họ Tên",
                },
              ]}
            >
              <Input
                className="focus:border-white  text-gray-900 text-sm rounded-lg  p-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white"
                placeholder="nhập họ tên ..."
              />
            </Form.Item>
            <div className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</div>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Nhập email",
                },
              ]}
            >
              <Input
                className="focus:border-white  text-gray-900 text-sm rounded-lg  p-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white"
                placeholder="nhập email ..."
              />
            </Form.Item>
            <div className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white">Phone number</div>
            <Form.Item
              name="soDt"
              rules={[
                {
                  required: true,
                  message: "Nhập số điện thoại",
                },
              ]}
            >
              <Input
                className="focus:border-white  text-gray-900 text-sm rounded-lg  p-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white"
                placeholder="nhập số điện thoại ..."
              />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                span: 24,
              }}
            >
              <div class="flex justify-between items-start mb-6">
                <div className="flex">
                  <div class="flex items-center h-5">
                    <input
                      id="remember"
                      type="checkbox"
                      value=""
                      class="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800"
                      required
                    />
                  </div>

                  <label for="remember" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                    I agree to the terms
                  </label>
                </div>
                <Link to={PATH.login}>
                  <p className="text-bluebutton hover:text-white duration-200 cursor-pointer ">Login?</p>
                </Link>
              </div>
              <Button
                style={{ lineHeight: "5px" }}
                className="block w-full py-6 mb-5 text-white bg-bluebutton hover:bg-red-200"
                type="primary"
                htmlType="submit"
              >
                Đăng ký
              </Button>

              <p className=" text-bluebutton hover:text-white duration-200 cursor-pointer text-center">
                Terms and Conditions
              </p>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
};

export default Register;
