import React, { useEffect } from "react";

import { Button, Checkbox, Form, Input, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { userServ } from "../services/QuanLyNguoiDung";
import { localUserServ } from "../services/localService";
import { setLoginUser } from "../services/userSlice";
import { toast } from "react-toastify";

const Login = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let fillForm = () => {
    let info = localUserServ.get();
    if (info != null) {
      console.log(info);
      return info;
    } else {
      return { taiKhoan: "", matKhau: "" };
    }
  };

  
  useEffect(() => {
    // Kiểm tra xem có thông tin người dùng trong Local Storage hay không
    const userInfo = localUserServ.get();
    if (userInfo) {
      // Nếu có, chuyển hướng người dùng đến trang khác, ví dụ: trang chính
      navigate("/Userinfomation");
    }
  }, []);

  const onFinish = (values) => {
    userServ
      .loginUser(values)
      .then((res) => {
        toast.success("Welcome to MovieBooker", {
          position: "top-right",
          autoClose: 5000,
          style: {
            background: "#14161d",
            color: "white",
          },
        });
        dispatch(setLoginUser(res.data.content));
        localUserServ.set(res.data.content);
        // * sau 1.5s sẽ chuyển tới trang đăng nhập
        setTimeout(() => {
          navigate("/");
        }, 1500);
      })
      .catch((err) => {
        toast.error("Username or password is incorrect", {
          position: "top-right",
          autoClose: 8000,
          style: {
            background: "#14161d",
            color: "white",
          },
        });
        console.log(err);
      });
  };
  const onFinishFailed = (errInfo) => {
    console.log("Failed:", errInfo);
  };
  return (
    <>
      <div className="max-w-[70%] flex mr-auto my-20 justify-center mx-auto rounded-2xl bg-[#14161d] p-5">
        <div className=" flex ">
        <img className="w-[500px] " src="./img/img/Kerfin7_NEA_2581.png" alt="" />
        </div>
        <div className="w-1/2">
          <div className=" mt-10 ">
            <h2 className="pt-5 flex justify-center my-10   text-3xl font-bold text-white">Welcome! Please log in</h2>
          </div>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
              taiKhoan: fillForm().taiKhoan,
              matKhau: fillForm().matKhau,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
            className="space-y-8  justify-center mx-auto pb-10   "
          >
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please enter your password",
                },
              ]}
            >
              <Input
                className="focus:border-white mb-5   text-gray-900 text-sm rounded-lg  p-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white"
                placeholder="User"
              />
            </Form.Item>

            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please enter your account",
                },
              ]}
            >
              <Input
                className="focus:border-white mb-5   text-gray-900 text-sm rounded-lg  p-3 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white"
                placeholder="Password"
                type="password"
              />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button
                type="primary"
                htmlType="submit"
                className="w-full p-5 mb-5 flex text-center justify-center items-center mr-auto text-white bg-blue-500"
              >
                Login
              </Button>
              <div className="flex justify-between">
                <Form.Item name="remember" valuePropName="checked">
                  <Checkbox className="text-white  ">Save Account</Checkbox>
                </Form.Item>
                <NavLink to={"/register"}>
                  <span className="text-bluebutton hover:text-white duration-200 cursor-pointer m   ">
                    If you don't have an account, please register
                  </span>
                </NavLink>
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
};

export default Login;
