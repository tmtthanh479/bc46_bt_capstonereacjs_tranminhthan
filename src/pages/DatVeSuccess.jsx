import React from "react";

const DatVeSuccess = () => {
  return (
    <div className="my-20">
      <div>
        <div className="text-MaxHeader h-[700px] font-bold bg-[#14161d]  text-center justify-center items-center rounded-xl p-5 max-w-[70%] flex  mx-auto">
          <p>
            You have <span className="text-red1 "> successfully </span> booked your tickets{" "}
            <span className="flex font-thin text-center justify-center items-center text-gray-300  text-ContentMin">
              {" "}
              Go to 'My Tickets' to view them. Enjoy your movie experience!"
            </span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default DatVeSuccess;
