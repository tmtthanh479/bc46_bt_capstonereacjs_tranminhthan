import React, { useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const TabComponent = () => {
  const [activeTab, setActiveTab] = useState("tab1");

  const openTab = (tabId) => {
    setActiveTab(tabId);
  };

  const sliderSettings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div>
      <div className="tabs">
        <button
          className={`tab-button ${activeTab === "tab1" ? "active" : ""}`}
          onClick={() => openTab("tab1")}
        >
          Tab 1
        </button>
        <button
          className={`tab-button ${activeTab === "tab2" ? "active" : ""}`}
          onClick={() => openTab("tab2")}
        >
          Tab 2
        </button>
        <button
          className={`tab-button ${activeTab === "tab3" ? "active" : ""}`}
          onClick={() => openTab("tab3")}
        >
          Tab 3
        </button>
      </div>

      {activeTab === "tab1" && (
        <Slider {...sliderSettings}>
          <div>Nội dung của Tab 1</div>
        </Slider>
      )}

      {activeTab === "tab2" && (
        <Slider {...sliderSettings}>
          <div>Nội dung của Tab 2</div>
        </Slider>
      )}

      {activeTab === "tab3" && (
        <Slider {...sliderSettings}>
          <div>Nội dung của Tab 3</div>
        </Slider>
      )}
    </div>
  );
};

export default TabComponent;
