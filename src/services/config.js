import axios from "axios";

const baseURL = "https://movienew.cybersoft.edu.vn";
const TokenCyberSoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0NiIsIkhldEhhblN0cmluZyI6IjMxLzAxLzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcwNjY1OTIwMDAwMCIsIm5iZiI6MTY3ODI5NDgwMCwiZXhwIjoxNzA2ODA2ODAwfQ.RmFBx9ElL7VuYNzZnzMoGUHyC3iXKRpw7Yvq2LsXk0Q";
const configHeader = () => {
  return {
    // * Nơi chứa token học viên
    TokenCyberSoft: TokenCyberSoft,
    // * nơi chứa token access của acount (tạo ra sau khi đăng nhập tài khoản thành công)
  };
};

// TODO: sử dụng axios trung gian để gọi api

export const https = axios.create({
  baseURL: baseURL,
  headers: configHeader(),
});
