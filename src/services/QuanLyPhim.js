import axios from "axios";

const baseURL = "https://movienew.cybersoft.edu.vn/api/QuanLyPhim";
const tokenCybersoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0NiIsIkhldEhhblN0cmluZyI6IjMxLzAxLzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcwNjY1OTIwMDAwMCIsIm5iZiI6MTY3ODI5NDgwMCwiZXhwIjoxNzA2ODA2ODAwfQ.RmFBx9ElL7VuYNzZnzMoGUHyC3iXKRpw7Yvq2LsXk0Q";

const instance = axios.create({
  baseURL,
  headers: {
    tokenCybersoft,
  },
});

export const getBanner = () => {
  return instance.get("/LayDanhSachBanner");
};

export const getMovies = () => {
  return instance.get(`/LayDanhSachPhim?maNhom=GP01`);
};
export const getDetail = () => {
  return instance.get(`/LayDanhSachPhimPhanTrang?maNhom=GP01&soTrang=1&soPhanTuTrenTrang=10`);
};
export const getRap = () => {
  return instance.get(`https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinHeThongRap`);
};
export const getlocationBHD = () => {
  return instance.get(`https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=BHDStar`);
};
export const getlocationCGV = () => {
  return instance.get(`https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=cgv`);
};
export const getlocationCineStar = () => {
  return instance.get(`https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=CineStar`);
};
export const getlocationCineGalaxy = () => {
  return instance.get(`https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=galaxy`);
};
export const getlocationCineLotteCinima = () => {
  return instance.get(`https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=LotteCinima`);
};
export const getlocationCineMegaGS = () => {
  return instance.get(`https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=MegaGS`);
};
export const gettimecBHD = () => {
  return instance.get(`https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=BHDStar&maNhom=GP01`);
};


