import axios from "axios";

const baseURL = "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung";

const tokenCybersoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0NiIsIkhldEhhblN0cmluZyI6IjMxLzAxLzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcwNjY1OTIwMDAwMCIsIm5iZiI6MTY3ODI5NDgwMCwiZXhwIjoxNzA2ODA2ODAwfQ.RmFBx9ElL7VuYNzZnzMoGUHyC3iXKRpw7Yvq2LsXk0Q";
const instance = axios.create({
  baseURL,
  headers: {
    tokenCybersoft,
  },
});

export const getregister = () => {
  return instance.get("/DangKy");
};
