import { https } from "./config"

export const userServ = {
    // * chứa các phương thức liên quan tới user tương tác với website 
    registerUser : (userInfo) => {
        return https.post('/api/QuanLyNguoiDung/DangKy' , userInfo) ; 
    } , 
    // * test thử chức năng login user 
    loginUser : (values) => {
        return https.post('/api/QuanLyNguoiDung/DangNhap' , values) ; 
    }
}