import { combineReducers } from "redux";
import { movieReducerToolkit } from "./Movie/slice";
import { handleSuccess } from "./Movie/formSuccessSlice";






export const rootReducer = combineReducers({

  movie: movieReducerToolkit,
  formSuccessSlice: handleSuccess,


});
