import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  Booking: [],
  Booked: [],
  InforFilmRap: [],
  InforFilmLocation: [],
  setBookinfoFilmTime: [],
  productList: [],
};

const MovieSlinece = createSlice({
  name: "movie",
  initialState,
  reducers: {
    setchairBooking: (state, action) => {
      const index = state.Booking.findIndex((e) => e.soGhe === action.payload.soGhe);
      if (index !== -1) {
        // -1 đã tồn tại
        state.Booking.splice(index, 1);
      } else {
        state.Booking.push(action.payload);
      }
    },
    setDelBooking: (state, action) => {
      state.Booking = [];
    },
    setChairBooked: (state, { payload }) => {
      state.Booked = [...state.Booked, ...state.Booking];
      state.Booking = [];
    },
    setBookinfoFilmRap: (state, action) => {
      state.InforFilmRap.push(action.payload);
    },

    setBookinfoFilmLoacation: (state, action) => {
      state.InforFilmLocation.push(action.payload);
    },
    setBookinfoFilmTime: (state, action) => {
      state.setBookinfoFilmTime.push(action.payload);
    },
    addProduct: (state, action) => {
      state.productList.push(action.payload);
    },
  },
});

export const { actions: movieActions, reducer: movieReducerToolkit } = MovieSlinece;
